# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=4
inherit autotools eutils toolchain-funcs

DESCRIPTION="Basic system utilities for personal use"
HOMEPAGE="http://dev.dblaci.hu/dblaci-utils"
#nincs verziózva a letöltés
#SRC_URI="https://release.gitkraken.com/linux/gitkraken-amd64.tar.gz"
SRC_URI="https://release.gitkraken.com/linux/v${PV}.tar.gz"

#LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 x86"
S=${WORKDIR}"/gitkraken"
#IUSE="lr0"

RDEPEND="dev-libs/nss
	x11-libs/libnotify
	x11-libs/libXtst
	media-libs/alsa-lib
	gnome-base/libgnome-keyring
	gnome-base/gconf
	"
DEPEND="${RDEPEND}
	"


INSTALL_DIR="/opt/gitkraken"

src_install() {

	insinto "${INSTALL_DIR}"
#	doins -r GitKraken/*
	doins -r .

	fperms 0755 ${INSTALL_DIR}/gitkraken

	dosym ${INSTALL_DIR}/gitkraken /usr/bin/gitkraken
#	doins -r . || die "doins failed"
#	fperms 0600 /etc/mysql_admin_config.php
#	fperms 0600 /etc/mysql_replication_config.php

#	dobin usr/bin/*
#	dosbin usr/sbin/*
#	rm ${D}/usr/sbin/dev_config.php
#	fperms 0700 /usr/sbin/service-status.php

#        # create the environment
#        local envd="${T}/90vmware"
#        cat > "${envd}" <<-EOF
#                PATH='${VM_INSTALL_DIR}/bin'
#                ROOTPATH='${VM_INSTALL_DIR}/bin'
#        EOF
#        use bundled-libs && echo 'VMWARE_USE_SHIPPED_LIBS=1' >> "${envd}"
#
#        doenvd "${envd}"
	

}
