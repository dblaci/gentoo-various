# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/www-apache/mod_authn_sasl/mod_authn_sasl-1.2.ebuild,v 1.1 2011/10/14 17:27:00 beandog Exp $

inherit eutils

DESCRIPTION="APE Project"
HOMEPAGE="http://www.ape-project.org/"
SRC_URI="http://dblaci.hu/gentoo/APE-Project-APE_Server-v1.1.0-21-gf0cac70.tar.gz"
S="${WORKDIR}/APE-Project-APE_Server-f0cac70"

#LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

#DEPEND="dev-libs/cyrus-sasl"
RDEPEND="${DEPEND}"

#src_unpack {
#
#}


src_compile() {
    ${S}/build.sh
#    make install
#    die
}

src_install() {
    dobin bin/aped
}
