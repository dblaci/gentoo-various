# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit autotools toolchain-funcs

DESCRIPTION="Basic system utilities for personal use"
HOMEPAGE="http://dev.dblaci.hu/dblaci-utils"
SRC_URI="http://dev.dblaci.hu/gentoo/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 x86"
S=${WORKDIR}/${PN}
#IUSE="lr0"

RDEPEND="dev-lang/php
         sys-apps/pv
        "
DEPEND="${RDEPEND}
        "

src_install() {
        doins -r . || die "doins failed"
        fperms 0600 /etc/mysql_admin_config.php
        fperms 0600 /etc/mysql_replication_config.php

        dobin usr/bin/*
        dosbin usr/sbin/*
#       rm ${D}/usr/sbin/dev_config.php
#       fperms 0700 /usr/sbin/service-status.php
}
