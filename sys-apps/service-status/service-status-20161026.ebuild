# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit autotools toolchain-funcs

DESCRIPTION="Monitor system status"
HOMEPAGE="http://dev.dblaci.hu/service-status"
SRC_URI="http://dev.dblaci.hu/gentoo/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 x86"
S=${WORKDIR}/${PN}
#IUSE="lr0"

RDEPEND="dev-lang/php[sockets,pcntl]
        >=sys-apps/dblaci-utils-0.14
        "
DEPEND="${RDEPEND}
        "

src_install() {
        doins -r . || die "doins failed"
        fperms 0640 /etc/service-status/config.php

        newinitd etc/init.d/service-status service-status
        dobin usr/bin/service-status.php
        dosbin usr/sbin/service-daemon.php
#       rm ${D}/usr/sbin/dev_config.php
#       fperms 0700 /usr/sbin/service-status.php
}
